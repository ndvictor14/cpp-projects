#include <iostream>
#include <ctime>
#include <sstream>
#include <cmath>
#include <cstdlib>
#include <iomanip>
using namespace std;

int get_day_of_week(int,int,int);
int format = 0, firstDay = 0;  // dayFormat and firstDay are added for format customization

bool isleap (int year)
{

  if ( ( !(year % 4) && (year % 100) ) || !(year % 400) )
    return (true);
  else
    return (false);
}

int number_of_month_days(int month, int year)
{
  // [Jan (1), Mar (3), May (5), Jul (7), Aug (8), Oct (10), Dec (12)] - 31, Feb(2) 28 if !isleap - 29 if isleap, [ Apr (4), Jun (6), Sept (9), Nov (11)] - 30

  // 31 Day months
  if ( month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12 ) {
    return 31;
  }

  // February
  else if ( month == 2 ){
      if ( isleap (year) ){
	// leap year
	return 29;
      }
      else {
	return 28;
      }
  }

  // 30 Day Months
  else if ( month == 4 || month == 6 || month == 9 || month == 11 ){
    return 30;
  }
}

int number_of_days_in_a_year(int year)
{

  if (isleap){
    return 366;
  }
  else{
    return 365;
  }
}

int get_first_day_of_year (int year)
{
  double dyear=(double) year;
  int first_day_of_year;
  first_day_of_year = ( ((year - 1) * 365) + ( (int) floor((dyear - 1) /
							   4) ) + ( (int) (floor(dyear - 1) / 100 )) + ( (int) floor((dyear - 1)
														     / 400 )))%7;

  return first_day_of_year;

}

int get_first_day_of_month (int month, int year)
{
  
  // We need to figure out which week day is the first of the specified month
  // Since each month starts with the first this is fairly straighforward, we'll use the get_day_of_week function and pass in our month, year, and 1 for the day
  int day_one = get_day_of_week( month, 1, year );

  return day_one;

}


string get_month_name(int month)
{

  //Code here
  if ( month == 1 ){
    return "January";
  }
  else if ( month == 2 ){
    return "February";
  }
  else if ( month == 3 ){
    return "March";
  }
  else if ( month == 4 ){
    return "April";
  }
  else if ( month == 5 ){
    return "May";
  }
  else if ( month == 6 ){
    return "June";
  }
  else if ( month == 7 ){
    return "July";
  }
  else if ( month == 8 ){
    return "August";
  }
  else if ( month == 9 ){
    return "September";
  }
  else if ( month == 10 ){
    return "October";
  }
  else if ( month == 11 ){
    return "November";
  }
  else if ( month == 12 ){
    return "December"; 
  }
  else{
    return "Bad";
  }

}


string get_day_of_week_short_name(int weekDaynum)
{
  // start days at 0 
  if ( weekDaynum == 0 ){
    return "Mo";
  }

  else if ( weekDaynum == 1 ){
    return "Tu";
  }

  else if ( weekDaynum == 2 ){
    return "We";
  }
  else if ( weekDaynum == 3 ){
    return "Th";
  }
  else if ( weekDaynum == 4 ){
    return "Fr";
  }
  else if ( weekDaynum == 5 ){
    return "Sa";
  }
  else if ( weekDaynum == 6 ){
    return "Su";
  }
  else{
    return "BAD";
  }
}

string get_day_of_week_name(int weekDaynum)
{
  // start days at 0 
  if ( weekDaynum % 7 == 0 ){
    return "Monday";
  }

  else if ( weekDaynum % 7 == 1 ){
    return "Tuesday";
  }

  else if ( weekDaynum % 7 == 2 ){
    return "Wednesday";
  }
  else if ( weekDaynum % 7 == 3 ){
    return "Thursday";
  }
  else if ( weekDaynum % 7 == 4 ){
    return "Friday";
  }
  else if ( weekDaynum % 7 == 5 ){
    return "Saturday";
  }
  else if ( weekDaynum % 7 == 6 ){
    return "Sunday";
  }
  else{
    return "BAD";
  }
}


int get_day_of_week2(int month, int day, int year) //Find the Day of the week
{
  //Uspensky and Heaslet gave the following formula, in Elementary Number Theory, 1939, for the Gregorian calendar:

    // W=D+floor(2.6m-0.2)+y+floor(y/4)+floor(c/4)-2c (mod 7)

    int weekDay,century;

  century=year/100;
  weekDay=(day+((int) floor(2.6*month-0.2)+year)+(int)
	   floor(year/4.0)+((int) floor(century/4.0)-2*century))%7;

  return weekDay;
}


int get_day_of_week(int month, int day, int year) //Find the Day of the Week
{

  int weekDay;
  if (year < 0) //invalid year
    return 0;
  else
    {
      if (month < 3) //Jan = 13 & Feb = 14 and year is the previous year
	{
	  month += 12;
	  year  -= 1;
	}
      weekDay = (day + 2*month + 3*(month+1)/5 + year +
		 year/4 - year/100 + year/400 + 1) % 7;

      if (month > 12)
	{ //reset Jan & Feb back to 1 &2
	  month -= 12;
	  year  += 1 ;
	}


    }
  return weekDay;
}

int day_of_year(int month, int day, int year)
{
  // Return the day of the year
  int thisDay = 0; // This is what will be returned on this day
  for ( int i = 0 ; i < month ; i++ ){
    day += number_of_month_days( i, year );
  }
  // day currently has all days up through our current month
  // Add the number of days it's been in our current month
  thisDay += day;
  return thisDay; // calculation complete let's return this bad boy
}


int day_of_century( int month, int day, int year ){
  // return the day in this century ( 2000 - 2999 )

  int centuryDay = 0;  // This should be a holiday. just saying. this will track our day in the current century. 
  for ( int yearStart = 2000; yearStart < year ; yearStart++ ){
    centuryDay += day_of_year( 12, 31, yearStart ); // Add days of past years
  }
  // Add this year's current date day
  centuryDay += day_of_year( month, day, year );


  return centuryDay; // I wish I could return to 12-31-99. Life was so much easier in that century :(

}

int day_of_forever(int month, int day, int year)
{

  // Calculate the day in forever time ( from year 0 )
  int foreverDay = 0;
  for ( int currentYear = 0; currentYear < year; currentYear++ ){
    foreverDay += day_of_year( 12, 31, currentYear );
  }
  // Add this year's current date day
  foreverDay += day_of_year( month, day, year );

  return foreverDay; 

  
}


void write_day_names_console( )
{

  // format 0 = short, 1 = long
  int currentDay = firstDay;
  for ( int maxDay = 7 ; currentDay < maxDay ; currentDay++ ){
    if ( format == 0 ){
      cout << setw( 3 ) << get_day_of_week_short_name( currentDay );
    }
    else if ( format == 1 ){
      cout << setw( 10 ) << get_day_of_week_name( currentDay );
    }
  }
  for ( int lowestDay = 0 ; lowestDay < firstDay ; lowestDay++ ){
    if ( format == 0 ){
      cout << setw( 3 ) << get_day_of_week_short_name( lowestDay );
    }
    else if ( format == 1 ){
      cout << setw( 10 ) << get_day_of_week_name( lowestDay );
    }
  }
  cout << endl;

}


int sundays_in_year( int year ){
  int sundays = 0;
  int monthStart = 0;
  // Go through each month 
  for ( int currentMonth = 1 ; currentMonth <= 12 ; currentMonth++ ){
    monthStart = get_first_day_of_month( currentMonth, year ); // base for our counts --- This is the actual Day (Mon - Sun)
    // these are the count of days (not actual days [mon-sun])
    for( int currentDay = 1 ; currentDay <= number_of_month_days( currentMonth, year ); currentDay++){
      // Sunday is the '7th' day so we can count based off of this information
      if ( monthStart % 7 == 0){
	sundays++;
      }
      monthStart++;
    }
  }

  return sundays;

}

void draw_calendar_console(int month,int year)
{
  // we need to print out x blanks until we reach the first day of the month
  int monthStart = get_first_day_of_month( month, year ) - 1;

  // We need to accommodate for the option of starting at a different day
  int neededBlanks = 0;
  int tempDay = firstDay;
  while ( get_day_of_week_name( tempDay ) != get_day_of_week_name( monthStart ) ){
    neededBlanks++;
    tempDay++;
  }
    
  // Note: Assignment 6 requests input of year, month but function was preset as month, year -- will leave as is but may need to be adjusted. 
  write_day_names_console();
  int column = 0; // This will allow us to determine when to go to the next line


  for ( neededBlanks ; neededBlanks > 0 ; neededBlanks-- ){
    if ( format == 0 ){
      cout << setw(3) << " ";
    }
    else if ( format == 1 ){
      cout << setw(10) << " ";
    }
    column++;
  }
  
  // No we can start printing our dates
  int daysInMonth = number_of_month_days( month, year );
  for ( int thisDay = 1 ; thisDay <= daysInMonth ; thisDay++ ){
    // Since there are 7 days (columns) in a week, we'll brake when our counter reaches 7
    // make sure it's not 0 since it's valid for the first day of the week to be 0 meaning no blanks were needed
    if ( column % 7 == 0 && column != 0){
      cout << endl;
    }
    if ( format == 0 ){
    cout << setw(3) << thisDay;
    }
    else if ( format == 1 ){
      cout << setw(10) << thisDay;
    }
    column++;
  }

  cout << endl << endl; // make it pretty

  // Extra credit
  cout << "Sundays in "<< year << ": " << sundays_in_year(year) << endl;
}


// This function is used to help user when deciding whether to adjust defaults
void print_current_settings( ){
  string formatString;
  ( format == 0 ) ? formatString = "short (Mo)" : formatString = "long (Monday)";
  cout << "*****   Current Settings   *****" << endl;
  cout << "Day Format: "  << formatString << endl;
  cout << "First Day: ";
  if ( format == 0){
    cout << get_day_of_week_name( firstDay ) << endl;
  }
  else if ( format == 1 ){
    cout << get_day_of_week_short_name( firstDay ) << endl;
  }
}



int main()
{

  int month, year;
  bool more;
  char c;
  more=true;
  while (more)
    {
      cout << "********************Welcome to BTE320 Calendar*************************" << endl <<endl;
      // Options for modifying output
      cout << "Would you like to adjust your settings?" << endl;
      print_current_settings( );
      cout << "Yes (y) or No (n): ";
      cin >> c;
      if ( c == 'Y' || c == 'y' ){
	cout << "Date Format [0=Short (Mo), 1=Long (Monday)]: ";
	cin >> format;
	cout << "First Day [0=Monday, 1=Tuesday, 2=Wednesday, 3=Thursday, 4=Friday, 5=Saturday, 6=Sunday]: ";
	cin >> firstDay;
      }
      cout << "Please enter the year: ";
      cin >> year;
      cout << endl;
      cout << "Please enter the month: ";
      cin >> month;
      cout << endl;

      cout << "*************  " << get_month_name(month) << " " << year << " ************" << endl << endl;
      draw_calendar_console(month, year);
      cout << endl;
      cout << "Would you like to view another calendar?(Enter N to end the program): ";
      cin >> c;
      if (c=='N' || c=='n')
	{
	  more=false;
	  exit(0);
	}
    }
  return 0;
}
