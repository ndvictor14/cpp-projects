#include <iostream>
#include <cstdlib>
#include <time.h>
#include <string.h>

// Guessing game 0-100

int main (){
  srand(time(NULL)); // random seed

  // variables
  int randomNumber = rand() % 101; // random number from 0-100
  int guess; // user input 
  bool gameOver;
  int attempts = 0;
  std::cout << "I'm thinking of a number between 0 and 100. Can you guess it?" << std::endl;
  do {
    std::cout << "Guess (-1 to quit): ";
    std::cin >> guess;
    if (std::cin.fail()){
      std::cout << "Please enter only number. To quit enter -1." << std::endl;
      std::cin.clear();
      std::cin.ignore(1000,'\n');
    }
    else if (guess == -1){
      gameOver = true;
    }
    else if (guess > randomNumber){
      std::cout << "You've guessed to high. Try again!\n";
      attempts++;
    }
    else if (guess < randomNumber){
      std::cout << "You've guessed to low. Try again!\n";
      attempts++;
    }
    else if (guess == randomNumber){
      std::string answer;
      std::cout << "AH! You got it!" << std::endl;
      std::cout << "It only took you " << attempts << " tries!" << std::endl;
      std::cout << "Would you like to play again?" << std::endl;
      std::cout << "Yes or No ? ";
      std::cin >> answer;
      attempts++;

      if (answer == "Y" || answer == "Yes" || answer == "y" || answer == "yes" || answer == "YES"){
	gameOver = false;
	randomNumber = rand() % 101;
	attempts = 0;
      }
      else{
	gameOver = true;
      }
    }

  } while (!gameOver);
  std::cout <<  "Thanks for playing!"  << std::endl;
  return 0;
  
}
