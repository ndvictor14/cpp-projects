#include <iostream>

int main(){
  int number;
  // Accept a number, -1 to quit, don't error on non integer
  do {
    std::cout << "Enter a number (-1 to quit): ";
    std::cin >> number;
    if ( std::cin.fail() ){
      std::cin.clear();
      std::cin.ignore(10000,'\n');
      std::cout << "Looks like you had invalid characters. I removed those for you.\n";
    }
    else if (number != -1){
      std::cout << "You entered: " << number << std::endl;
    }
  } while (number != -1);
  std::cout << "All done!\n";
  return 0;
}
