#include <iostream>

int main(){
  int tally = 0;
  for (int book1 = 0 ; book1 < 6 ; book1++){
    for (int book2 = 1 ; book2 < 6 ; book2++){
      for (int book3 = 2 ; book3 < 6 ; book3++){
	for (int book4 = 3 ; book4 < 6 ; book4++){
	  for (int book5 = 4 ; book5 < 6 ; book5++){
	    for (int book6 = 5 ; book6 < 6 ; book6++){
	      tally++;
	    }
	  }
	}
      }
    }
  }
  std::cout << "Can be arranged " << tally << " ways." << std::endl;

  return 0;
}
