Hangman v1.0

The hangman game is able to keep track of user's score and allows the user to keep track of their wins, losses, and win to loss ration. It allows users to select a difficulty level. The game itself displays the hangman as well as a scoreboard and timer. 

Static:
  board.jpg - regular board
  board1.jpg - board with one piece
  board2.jpg - board with two pieces
  board3.jpg - board with three pieces
  board4.jpg - board with four pieces
  board5.jpg - board with five pieces
  board6.jpg - board with six pieces
  scoreboard.jpg - the score board
