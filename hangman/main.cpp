#include <iostream>
#include "SDL/SDL.h"

int main(){
  //images
  SDL_Surface *hello = NULL;
  SDL_Surface *screen = NULL;

  //Start SDL 
  SDL_Init(SDL_INIT_EVERYTHING);

  // Set up screen
  screen = SDL_SetVideoMode(640, 480, 32, SDL_SWSURFACE );

  //Load Image
  hello = SDL_LoadBMP("board.jpg");

  // Apply Image to screen
  SDL_BlitSurface (hello, NULL, screen, NULL);
  
  // Update screen
  SDL_Flip(screen);
 
  // Pause
  SDL_Delay( 2000 );

  // Free loaded image
  SDL_FreeSurface(hello);

  // Quit SDL
  SDL_Quit();

  return 0;
}
