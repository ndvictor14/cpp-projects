#include "Playlist.h"
using std::ostream;


void Playlist::addSong( Song newSong ){
  playlist.push_back( newSong );
}


void Playlist::display() {
  // we'll need a custom iterator for displaying our playlist
  std::list<Song>::iterator SongIterator;
  
  for (SongIterator = playlist.begin(); SongIterator != playlist.end(); ++SongIterator ){
    std::cout << "Song ID: " << *SongIterator;
  }
  std::cout << '\n';
}
