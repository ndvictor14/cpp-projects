#include "Song.h"

class Playlist{
 public:
  std::string PlaylistName;
  std::list<Song> playlist;
  void addSong( Song );
  void deleteSong( Song );
  Song loadSong( Song );
  void shuffle();
  void transfser( Song, Playlist );
  void display();
  void shuffleSongs();
  Song searchPlaylist( std::string );
  void sortByArtist();
  void sortByTitle();
 private:
  int numsongs;
  Time totalPlayTime;

};
