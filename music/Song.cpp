#include "Song.h"

int Song::nextID = 0;

Song::Song() {
  songID = ++nextID;
}

Song::Song(const Song& orig) {
  songID = orig.songID;
}

Song& Song::operator=(const Song& orig) {
  songID = orig.songID;
  return(*this);
}

std::string Song::getTitle( void ){
  return title;
}

std::string Song::getArtist( void ){
  return artist;
}

Time Song::getRuntime( void ){
  return runtime;
}

void Song::setTitle( std::string newTitle ){
  title = newTitle;
}

void Song::setArtist( std::string newArtist ){
  artist = newArtist;
}

void Song::setRunTime( Time songRunTime ){
  runtime = songRunTime;
}
