#include <iostream>
#include <string>
#include "time_struct.h"
#include <list>
using std::ostream;

class Song{
 public:
  std::string getTitle( void );
  std::string getArtist( void );
  Time getRuntime( void );
  // These methods are additional to allow for manual title / artist overwriting
  void setTitle( std::string );
  void setArtist( std::string );
  void setRunTime( Time );
  Song();
  Song(const Song& orig);
  Song& operator=(const Song& orig);
  friend ostream& operator << (ostream& os, const Song& m)
  {
    os<< m.songID << std::endl;
    return os ;
  }

 private:
  std::string title;
  std::string artist;
  Time runtime;
  int songID;
 protected:
  static int nextID;
};
