#include <iostream>
#include "time_struct.h"
#include "song.h"

void printTimeDetails( Time X ){
  std::cout << "Hours: " << X.hours << " Minutes: " << X.minutes << " Seconds: " << X.seconds << std::endl;
}
int main() {
  std::cout << "Hello!" << std::endl;
  Time test1;
  test1.hours = 1;
  test1.minutes = 01;
  test1.seconds = 1;
  Song track1( "Hello World", "Awesome Programmer", test1 );
  std::cout << track1.getTitle() << std::endl;
  
  return 0;
}
