#include "song.h"

Song::Song( std::string songTitle, std::string songArtist, Time songRuntime ){
  // Song parameterized constructor
  title = songTitle;
  artist = songArtist;
  runtime = songRuntime;
}

std::string Song::getTitle( void ){
  return title;
}

std::string Song::getArtist( void ){
  return artist;
}

Time Song::getRuntime( void ){
  return runtime;
}

void Song::setTitle( std::string newTitle ){
  title = newTitle;
}

void Song::setArtist( std::string newArtist ){
  artist = newArtist;
}
