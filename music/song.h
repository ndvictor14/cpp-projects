#include <iostream>
#include <string>
#include "time_struct.h"

class Song{
 public:
  std::string getTitle( void );
  std::string getArtist( void );
  Time getRuntime( void );
  // These methods are additional to allow for manual title / artist overwriting
  void setTitle( std::string );
  void setArtist( std::string ); 
  Song( std::string, std::string, Time ); // constructor
  
 private:
  std::string title;
  std::string artist;
  Time runtime;

  
};
