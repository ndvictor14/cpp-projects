#include <iostream>
#include "time_struct.h"
#include "Playlist.h"

void printTimeDetails( Time X ){
  std::cout << "Hours: " << X.hours << " Minutes: " << X.minutes << " Seconds: " << X.seconds << std::endl;
}

void expectedTimeOutput( int input, int expected ){
  if ( input == expected ) {
    std::cout << "Passed" << std::endl;
  }
  else{
    std::cout << "Failed!\nReceived: " << input << "\nExpected: " << expected << std::endl;
  }    
}

void expectedStringOutput( std::string input, std::string expected ){
  if ( input == expected ) {
    std::cout << "Passed" << std::endl;
  }
  else{
    std::cout << "Failed!\nReceived: " << input << "\nExpected: " << expected << std::endl;
  }    

}
int main() {
  Time test1;
  test1.hours = 1;
  test1.minutes = 01;
  test1.seconds = 1;
  Song track1;
  track1.setTitle( "Hello World" );
  track1.setArtist( "Awesome Programmer" );
  track1.setRunTime( test1 );
  std::cout << track1.getTitle() << std::endl;
  track1.setTitle( "Hello Awesome Programmer" );
  expectedStringOutput( track1.getTitle(), "Hello Awesome Programmer" );

  Playlist myPlaylist;
  myPlaylist.display();
  Song mySong;
  mySong.setTitle( "Hello" );
  mySong.setArtist( "It's Me" );
  mySong.setRunTime( test1 );

  myPlaylist.addSong( track1 );
  myPlaylist.addSong( track1 );
  myPlaylist.addSong( mySong );
  myPlaylist.display();
  return 0;
}
