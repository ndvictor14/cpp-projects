#include "time_struct.h"
//#include <iostream>
// streams used for verbosity, if need to debug it can be uncommented


Time Time::add_times( Time One, Time Two){
  // add_times implementation
  // Take two times and return the sum of the two in a Time struct

  // variables we'll be using
  int totalSeconds = 0;
  int totalMinutes = 0;
  int totalHours = 0;

  // Add seconds together
  totalSeconds = One.seconds + Two.seconds;

  // 60 seconds = 1 minute
  totalMinutes = totalSeconds / 60;

  // total seconds are what's left after we take the minute if applicable
  totalSeconds %= 60;

  // now we add the minutes
  totalMinutes += One.minutes + Two.minutes;

  // 60 minutes = 1 hour
  totalHours = totalMinutes / 60;

  // total minutes are what's left after we take the hour
  totalMinutes %= 60;

  // finally, we just need the hours
  //std::cout << "current hours: " << totalHours << std::endl;
  //std::cout << "Adding " << One.hours << " and " << Two.hours << std::endl;
  totalHours += One.hours + Two.hours;

  // now we can return our Time struct
  Time myResults;

  myResults.hours = totalHours;
  myResults.minutes = totalMinutes;
  myResults.seconds = totalSeconds;

  return myResults;
  
}
