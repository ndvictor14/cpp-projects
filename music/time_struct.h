#ifndef TIME_H
#define TIME_H

struct Time{
  int hours;
  int minutes;
  int seconds;
  Time add_times( Time, Time ); // add two times together return new Time 

};

#endif
