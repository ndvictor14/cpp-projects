#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>

/*
Description: This is a simple command line tic tac toe game
Author: Victor Hernandez
*/


  std::string Players[2];

// mainMenu : A simple main menu
int mainMenu(){
  int choice;
  std::cout << std::setw(3) << "Tic-Tac-Toe" << std::endl;
  std::cout << std::setw(5) << "1 - Single Player" << std::endl;
  std::cout << std::setw(5) << "2 - Multiplayer" << std::endl;
  std::cout << std::setw(5) << "3 - Highscores" << std::endl;
  std::cout << std::setw(5) << "4 - Help" << std::endl;
  std::cout << std::setw(5) << "5 - Quit" << std::endl;
  std::cin >> choice;
  return choice;
};


// printBoard : Function for printing the board
void printBoard (char myBoard[3][3]){
  for (int i = 0 ; i < 3 ; i++){
    for (int j = 0 ; j < 3 ; j++){
      std::cout << myBoard[j][i];
      if (j < 2){
	std::cout << " | ";
      }
    }
    std::cout << std::endl;
  }
};

// clearBoard : Function for cleaning the board
void clearBoard (char myBoard[3][3]){
  for (int i = 0 ; i < 3 ; i++){
    for (int j = 0 ; j < 3 ; j++){
      myBoard[i][j] = '*';
    }
  }
};

// printScore : Function for printing player scores
void printScore(std::string P1, int score1, std::string P2, int score2){
  std::cout << std::setw(20);
  std::cout << P1 << ": " << std::setw(5) << score1 << std::endl;
  std::cout << "--------------------------------" << std::endl;
  std::cout << std::setw(20);
  std::cout << P2 << ": " << std::setw(5) << score2 << std::endl;
  std::cout << std::endl;
};

// checkBoard : Function for checking if a win condition is met
int checkBoard(char board[3][3]){
  // check horizontal victories
  for (int i = 0 ; i < 3 ; i++){
    for (int j = 0 ; j < 3 ; j++){
      if (board[i][j] != '*' && board[i][j] == board[i][j+1] && board[i][j+1] == board[i][j+2]){
	return 1;
      }
      else if (board[i][j] != '*' && board[i][j] == board[i+1][j] && board[i+1][j] == board[i+2][j]){
	return 1;
      }
    }
    if ( (board[0][0] != '*' && board[0][0] == board[1][1] && board[1][1] == board[2][2] ) || (board[2][0] != '*' && board[2][0] == board[1][1] && board[1][1] == board[0][2]) ){
      return 1;
    }
    return 0;
  }
};

void help(){
  std::fstream helpfile;
  std::string line;
  helpfile.open(".help.txt", std::fstream::in);
  if (helpfile.is_open())
    {
      while ( getline (helpfile,line) )
	{
	  std::cout << line << std::endl;
	}
      helpfile.close();
    }

  else std::cout << "Unable to open file" << std::endl; 
  helpfile.close();

}

void play(int players){
  Players[1] = "Computer Bot Man";
  for (int i = 0 ; i < players ; i++){
    std::cout << "Player " << i + 1 << ", enter your name: ";
    std::cin >> Players[i];
  }
  
};


int main(){
  int input;
  int p1Score = 0;
  int p2Score = 0;
  char board[3][3];
  bool gameOver = 0;

  input = mainMenu();
  switch (input){
  case 1:
    play(1);
    break;
  case 2:
    play(2);
    break;
  case 3:
    break;
  case 4:
    help();
    mainMenu();
  case 5:
    return 0;
  }
  clearBoard(board);
  printBoard(board);
  printScore(Players[0],p1Score,Players[1],p2Score);
}
